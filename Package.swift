// swift-tools-version:4.2

import PackageDescription



let package = Package(
	name: "test_crash_vapor",
	dependencies: [
		.package(url: "https://github.com/vapor/crypto.git", from: "3.0.0")
	],
	targets: [
		.target(name: "test_crash_vapor", dependencies: ["Crypto"])
	]
)
