import Crypto

let passphrase = "abcdef"

// From https://www.googleapis.com/oauth2/v3/certs
let key = try RSAKey.components(n: "vvAaaSpfr934Qx0ioFiWsopq7UCfLNn0zjYVbq4bvUcGSXU9kowYmQArR7WlIkjk1moffla0UV75QRaQPATva1oD5xQnnW-20haeMWTSsMgUHoN0Np9AD8ffPz-DfMJBOHIo4REL1BFFS33HSZgPl0hxJ-5UScqr4lW1JMy5XGeRho30dnmKTpakU1Oc35hFYKSea_O2SXfmbqiAkWlWkilEzgHq4pzVWiDZe4ZgfMdD4vqkSNrO_PkBFBT1mnBJztQ1h4v1jvUW-zeYYwIcPTaOX-xOTiGH9uQkcNPpe5pBrIZJqR5VNrDl_bJOmvVlhhXZSn4fkxA8kyQcZXGaTw", e: "AQAB")
let encryptedPassphrase = try RSA.encrypt(passphrase, padding: .pkcs1, key: key)
try print(RSA.decrypt(encryptedPassphrase, padding: .pkcs1, key: key))
